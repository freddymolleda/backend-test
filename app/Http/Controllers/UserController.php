<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\User;
use App\HobbyUser;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('hobbies')->get();

        return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'names'    => 'required|max:200',
            'surnames' => 'required|max:200',
            'email'    => 'required|max:50|email|unique:users',
            'password' => 'required|max:30|min:6|confirmed',
            'phone'    => 'required|max:20',
            'address'  => 'required|max:255',
        ];

        $this->validate($request, $rules);

        $fields= $request->all();
        $fields['password']             = bcrypt($request->password);
        $fields['verified']             = User::USUARIO_NO_VERIFICADO;
        $fields['verificaion_token']    = User::generarVerificationToken();
        $fields['admin']                =  User::USUARIO_REGULAR;
        
        $user = User::create($fields);

        if ($user) {
            $dataInsert = [];
            $ArrayHobbies = explode(',', $request->hobbies);
            foreach ($ArrayHobbies as $key => $value) {
                $dataInsert[]=[
                    'hobby_id' => $value,
                    'user_id' => $user->id,
                    'created_at' => date('Y-m-d H:m:s')
                ];
            }
            $hobbyUser = HobbyUser::insert($dataInsert);
        }

        return $this->showOne($user,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with('hobbies')->first();
        
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {   
        $rules = [
            'email' => 'email|max:50|unique:users,email,' . $user->id,
            'password' => 'min:6|max:30|confirmed',
            'admin' => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR,
        ];

        $this->validate($request, $rules);
        if ($request->has('names')) {
            $user->names =$request->names;
        }
        if ($request->has('email') && $user->email != $request->email  ) {
            
                $user->verified = User::USUARIO_NO_VERIFICADO;
                $user->verification_token = User::generarVerificationToken();
                $user->email = $request->email;
            
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }
        if ($request->has('admin')) {
            if (!$user->esVerificado()) {
                return $this->errorResponse('Unicamene los usuarios verificados pueden cambiar su valor de administrador', 409);
            }

            $user->admin = $request->admin;
        }

        if (!$user->isDirty()) {
            return $this->errorResponse('se debe especificar al menos un valor diferente para actualizar',422);
        }

        if ($user->save()) {
            $dataInsert = [];
            $ArrayHobbies = explode(',', $request->hobbies);
               
            foreach ($ArrayHobbies as $key => $value) {
                $hobbyUser = HobbyUser::where(['user_id' => $user->id, 'hobby_id' => $value])->first();
                if (empty($hobbyUser)) {
                    $hobbyUser = new HobbyUser();
                    $hobbyUser->user_id = $user->id;
                    $hobbyUser->hobby_id = $value;
                    $hobbyUser->save();
                }
            }
        }

        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        
        return $this->showOne($user); 
    }
}
