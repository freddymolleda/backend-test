<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HobbyUser extends Model
{
    protected $table = 'hobby_user';
}
