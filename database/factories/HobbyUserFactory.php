<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\HobbyUser;
use App\User;
use App\Hobby;
use Faker\Generator as Faker;

$factory->define(HobbyUser::class, function (Faker $faker) use ($factory) {
    return [
        'user_id' => $factory->create(User::class)->id,
        'hobby_id' => $factory->create(Hobby::class)->id
    ];
});
