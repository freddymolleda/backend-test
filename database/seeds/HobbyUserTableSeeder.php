<?php

use App\HobbyUser;
use Illuminate\Database\Seeder;

class HobbyUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HobbyUser::class,5)->create();
    }
}
